---
title: About me
subtitle: GitLab Pages Demo
comments: false
---

This project shows how documentation can be published using GitLab Pages. There are examples of:
 1. code snippet rendering
 2. math samples
 3. sidebars
 

Please direct all questions to **Maame Efua Boham** 
